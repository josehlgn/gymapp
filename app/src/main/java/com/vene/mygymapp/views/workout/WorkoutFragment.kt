package com.vene.mygymapp.views.workout

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentResultListener
import androidx.navigation.fragment.navArgs
import com.vene.mygymapp.R
import com.vene.mygymapp.models.Workout
import java.util.*

class WorkoutFragment : Fragment() {

    private lateinit var viewModel: WorkoutViewModel
    private var workoutId : Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.workout_fragment, container, false)
//        val button = view.findViewById<FloatingActionButton>(R.id.save_workout_btn)
//        button.setOnClickListener{
//            findNavController().navigate(R.id.save_workout)
//        }
//        val reps: Spinner = view.findViewById(R.id.number_of_repetitions)
//        ArrayAdapter.createFromResource(
//            this.requireContext(),
//            R.array.set_reps,
//            android.R.layout.simple_spinner_item
//        ).also { adapter ->
//            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//            reps.adapter = adapter
//        }
//
//        val days: Spinner = view.findViewById(R.id.day_list)
//        ArrayAdapter.createFromResource(
//            this.requireContext(),
//            R.array.days,
//            android.R.layout.simple_spinner_item
//        ).also { adapter ->
//            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//            days.adapter = adapter
//        }

        parentFragmentManager.setFragmentResultListener(
            "workout",
            this
        ) { requestKey: String,
            result: Bundle ->
            workoutId = result.getInt("workout")
            println(workoutId)
            getWorkoutById()
        }

        return view
    }

    private fun getWorkoutById() {

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(WorkoutViewModel::class.java)
        // TODO: Use the ViewModel

    }



}