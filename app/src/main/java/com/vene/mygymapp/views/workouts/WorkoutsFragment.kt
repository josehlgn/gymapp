package com.vene.mygymapp.views.workouts

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.vene.mygymapp.R
import com.vene.mygymapp.adapter.WorkoutAdapter
import com.vene.mygymapp.databinding.WorkoutsFragmentBinding
import com.vene.mygymapp.interfaces.APIService
import com.vene.mygymapp.models.Workout
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class WorkoutsFragment : Fragment() {

    private lateinit var viewModel: WorkoutsViewModel
    private lateinit var binding: WorkoutsFragmentBinding
    private lateinit var adapter: WorkoutAdapter
    private val workouts = mutableListOf<Workout>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = WorkoutsFragmentBinding.inflate(layoutInflater,container,false)
        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(WorkoutsViewModel::class.java)
        // TODO: Use the ViewModel
        initRecyclerview()
        listWorkouts()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu, menu)

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.add_workout -> {
            findNavController().navigate(R.id.to_create_new_workout)

            true
        } else -> {
            super.onOptionsItemSelected(item)
        }
    }

    private fun initRecyclerview() {
        adapter = WorkoutAdapter(workouts) {workout ->
            onItemSelected(
                workout
            )
        }
        binding.workoutRecyclerView.layoutManager = LinearLayoutManager(context)
        binding.workoutRecyclerView.adapter = adapter
    }

    fun onItemSelected(workout: Workout){
        Toast.makeText(context,workout.name,Toast.LENGTH_SHORT).show()
        parentFragmentManager.setFragmentResult("workout", bundleOf("workout" to workout.id))
        findNavController().navigate(R.id.to_create_new_workout)
    }

    private fun listWorkouts() {
        CoroutineScope(Dispatchers.IO).launch {
            val call = viewModel.getRetrofit().create(APIService::class.java).getWorkouts()
            val list = call.body()
            activity?.runOnUiThread {
                if (call.isSuccessful){
                    val exercises = list?.results ?: emptyList()
                    workouts.clear()
                    workouts.addAll(exercises)
                    adapter.notifyDataSetChanged()
                }else{
                    Toast.makeText(activity,call.message(),Toast.LENGTH_LONG).show()
                }
            }
        }
    }
}
