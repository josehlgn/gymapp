package com.vene.mygymapp.views.workouts

import androidx.lifecycle.ViewModel
import com.vene.mygymapp.interfaces.HeaderInterceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class WorkoutsViewModel : ViewModel() {

    private val client = OkHttpClient.Builder().apply {
        addInterceptor(HeaderInterceptor())
    }.build()

    fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://wger.de/api/v2/")
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

}