package com.vene.mygymapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vene.mygymapp.R
import com.vene.mygymapp.databinding.WorkoutItemBinding
import com.vene.mygymapp.models.Workout

class WorkoutAdapter (val workouts: List<Workout>, private val onClickListener: (Workout) -> Unit) : RecyclerView.Adapter<WorkoutsViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WorkoutsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return WorkoutsViewHolder(layoutInflater.inflate(R.layout.workout_item,parent,false))
    }

    override fun onBindViewHolder(holder: WorkoutsViewHolder, position: Int) {
        val item = workouts[position]
        holder.bind(item, onClickListener)
    }

    override fun getItemCount(): Int {
        return workouts.size
    }

}

class WorkoutsViewHolder(view : View) : RecyclerView.ViewHolder(view) {

    private val binding = WorkoutItemBinding.bind(view)

    fun bind(item: Workout, onClickListener: (Workout) -> Unit) {
        binding.workoutNameTV.text = item.name
        itemView.setOnClickListener {
            onClickListener(item)
        }
    }

}