package com.vene.mygymapp.interfaces

import com.vene.mygymapp.models.Exercises
import com.vene.mygymapp.models.Workouts

import retrofit2.Response
import retrofit2.http.GET

interface APIService {
    @GET("exercise/?limit=500&language=2")
    suspend fun getExercices(): Response<Exercises>

    @GET("workout/")
    suspend fun getWorkouts(): Response<Workouts>

    @GET("workout/{id}")
    suspend fun getWorkoutsById(id: Int): Response<Workouts>

}