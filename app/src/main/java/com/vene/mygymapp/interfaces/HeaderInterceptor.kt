package com.vene.mygymapp.interfaces

import okhttp3.Interceptor
import okhttp3.Response

class HeaderInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
            .newBuilder()
            .addHeader("Authorization", "Token 6740ca4dbf56ca46ae10456f4f78b5c6ed6b07a1")
            .build()
        return chain.proceed(request)
    }
}