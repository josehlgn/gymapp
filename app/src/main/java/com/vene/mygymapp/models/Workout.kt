package com.vene.mygymapp.models

import com.google.gson.annotations.SerializedName
import java.util.*

data class Workout(
    @SerializedName("id") var id: Int,
    @SerializedName("name") var name: String,
    @SerializedName("creation_date") var date: Date,
    @SerializedName("description") var description: String,
)
