package com.vene.mygymapp.models

import com.google.gson.annotations.SerializedName

data class Exercise(

    @SerializedName("id") var id: Int,
    @SerializedName("name") var name: String,
//    @SerializedName("uuid") var uuid: String,
//    @SerializedName("exercise_base_id") var exercise_base_id: Int,
//    @SerializedName("description") var description: String,
//    @SerializedName("category") var category: List<String>,
//    @SerializedName("muscles") var muscles: List<String>,
//    @SerializedName("muscles_secondary") var muscles_secondary: List<String>,
//    @SerializedName("equipment") var equipment: List<String>,
//    @SerializedName("variations") var variations: List<String>

)
