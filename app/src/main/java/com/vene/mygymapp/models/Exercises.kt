package com.vene.mygymapp.models

import com.google.gson.annotations.SerializedName

data class Exercises(
    @SerializedName("count") var count: Int,
    @SerializedName("next") var next: Int,
    @SerializedName("previous") var previous: Int,
    @SerializedName("results") var results: List<Exercise>)
